@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at->diffForHumans()}}</p>
			<p class="card-text d-inline">{{ $post->likes->count() }} likes |</p>
			<p class="card-text d-inline">{{ $post->comments->count() }} comments</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
					
				@endif
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
					Add Comment
				</button>
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	{{-- Comment Section for s05 Activity --}}
	<div class="mt-3">
		<h3>Comments</h3>
		@foreach ($post->comments as $comment)
		  <div class="card mb-3">
			<div class="card-body">
			  <p class="card-text">{{ $comment->content }}</p>
			  <div class="d-flex justify-content-between align-items-center">
				<small class="text-muted">{{ $comment->user->name }}</small>
				<small class="text-muted">{{ $comment->created_at->diffForHumans() }}</small>
			  </div>
			</div>
		  </div>
		@endforeach
	  </div>
	  


	{{-- Modal for s05 Activity (Posting comments) --}}
	
	  
	<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
			  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			  <form method="POST" action="/posts/{{$post->id}}/comment">
				@csrf
				<div class="mb-3">
				  <label for="content" class="form-label">Comment Content</label>
				  <textarea class="form-control" id="content" name="content" rows="3"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	  
@endsection