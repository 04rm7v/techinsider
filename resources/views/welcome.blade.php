@extends('layouts.app')
@section('content')
        <main class="py-4 container">
            {{-- @yield is used to define a section in a layout and is constantly used to get content from a child page into the master page--}}
            @yield('content')
            <div class="container-fluid text-center"><img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="h-25 w-50 "/></div>
            @if(count($posts) > 0)
                @foreach($posts as $post)
                    <div class="card text-center my-2">
                        <div class="card-body">
                            <h4 class="card-title mb-3">
                                <a href="/posts/{{$post->id}}">
                                    {{$post->title}}
                                </a>
                            </h4>
                            {{-- eloquent hava a specific functionality that automatically joins table, once the relationship is identified in the Models. --}}
                            <h6 class="card-text mb-3">
                                Author: {{$post->user->name}}
                            </h6>
                        
                        </div>
                    </div>
                @endforeach
            @else
                <div>
                    <h2>There are no post to show.</h2>
                    <a href="/posts/create" class="btn btn-info">Create post</a>
                </div>
            @endif
        </main>
    </div>
</body>
</html>
@endsection